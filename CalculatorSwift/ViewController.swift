//
//  ViewController.swift
//  CalculatorSwift
//
//  Created by Vadim Trulyaev on 7/1/15.
//  Copyright (c) 2015 vt. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var display: UILabel!
    
    var isUserInMiddleOfNumberEntering: Bool = false
    
    var brain = CalculatorBrain()

    @IBAction func digitPressed(sender: UIButton)
    {
        let digit = sender.currentTitle!
        
        if isUserInMiddleOfNumberEntering {
            display.text = display.text! + digit
        } else {
            display.text = digit
            isUserInMiddleOfNumberEntering = true
        }
    }
    
    @IBAction func operate(sender: UIButton) {
        if isUserInMiddleOfNumberEntering {
            enter()
        }
        if let operation = sender.currentTitle {
            if let result = brain.performOperation(operation) {
                displayValue = result
            } else {
                displayValue = 0
            }
        }
    }

    @IBAction func enter() {
        isUserInMiddleOfNumberEntering = false
        if let result = brain.pushOperand(displayValue) {
            displayValue = result
        } else {
            displayValue = 0
        }
    }
    
    var displayValue: Double {
        get {
            return NSNumberFormatter().numberFromString(display.text!)!.doubleValue
        }
        set {
            display.text = "\(newValue)"
        }
    }

}

