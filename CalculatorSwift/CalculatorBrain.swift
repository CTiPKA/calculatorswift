//
//  CalculatorBrain.swift
//  CalculatorSwift
//
//  Created by Vadim Trulyaev on 7/17/15.
//  Copyright (c) 2015 vt. All rights reserved.
//

import Foundation

class CalculatorBrain
{
    private enum Op: Printable
    {
        case Operand (Double)
        case UnaryOperation (String, Double -> Double)
        case BinaryOperation (String, (Double,Double) -> Double)
        
        var description :String {
            get {
                switch self {
                case .Operand(let operand):
                    return "\(operand)"
                case .UnaryOperation(let symbol, _):
                    return symbol
                case .BinaryOperation(let symbol, _):
                    return symbol
                }
            }
        }
    }
    
    private var opStack = [Op]()
    
    private var knownOps = [String:Op]()
    
    init () {
        knownOps["×"] = Op.BinaryOperation("×", *)
        knownOps["÷"] = Op.BinaryOperation("÷") {$1 / $0}
        knownOps["−"] = Op.BinaryOperation("−", -)
        knownOps["+"] = Op.BinaryOperation("+") {$0 + $1}
        knownOps["√"] = Op.UnaryOperation("√", sqrt)
    }
    
    private func evaluate(ops:[Op]) -> (result:Double?, remainingOps:[Op]) {
        
        if !ops.isEmpty {
            var remainingOps = ops
            let op = remainingOps.removeLast()
            
            switch op {
            case .Operand(let operand):
                return (operand,remainingOps)
            case .UnaryOperation(_, let operation):
                let operationEvaluate = evaluate(remainingOps)
                if let operand = operationEvaluate.result {
                    return (operation(operand),operationEvaluate.remainingOps)
                }
            case .BinaryOperation(_, let operation):
                let operationEvaluate1 = evaluate(remainingOps)
                if let operand1 = operationEvaluate1.result {
                    let operationEvaluate2 = evaluate(operationEvaluate1.remainingOps)
                    if let operand2 = operationEvaluate2.result {
                        return (operation(operand1,operand2),operationEvaluate2.remainingOps)
                    }
                }
            }
        }
        
        return (nil,ops)
    }
    
    func evaluate() -> Double? {
        let (result,remainder) = evaluate(opStack)
        println("\(opStack) = \(result) and reminder \(remainder)")
        return result
    }
    
    
    func pushOperand(operand:Double) -> Double? {
        opStack.append(Op.Operand(operand))
        return evaluate()
    }
    
    func performOperation(symbol:String) -> Double? {
        if let operation = knownOps[symbol] {
            opStack.append(operation)
        }
        return evaluate()
    }

}